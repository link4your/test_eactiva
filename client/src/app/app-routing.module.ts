import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from '../app/dashboard/dashboard/dashboard.component';
import { dashboardRoutesModule} from '../app/dashboard/dashboard/dashboard.routes.module';
import { AuthGuardService } from '../app/auth/auth-guard.service';
import { LoginComponent } from '../app/auth/login/login.component';


const routes: Routes = [
  {path: '', component: DashboardComponent, children: dashboardRoutesModule},
  {path: 'login', component: LoginComponent} 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
