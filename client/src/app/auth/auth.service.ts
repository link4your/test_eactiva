import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  sub
  constructor(private router: Router, private http: HttpClient) {
  }


  login(user: any) {
  return  this.http.post(environment.url + 'user/login', user);
  }

  logout() {
    localStorage.clear();
    this.router.navigate(['/']);
  }


}

