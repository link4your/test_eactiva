import { AddComponent } from '../../feeds/add/add.component';
import { Routes } from '@angular/router';
import { ListComponent } from 'src/app/feeds/list/list.component';
import { AuctionComponent } from 'src/app/feeds/auction/auction.component';



export const dashboardRoutesModule: Routes = [
      { path: 'auction/add', component: AddComponent },
      { path: 'auction/add/:id', component: AddComponent },
      { path: 'auction/list', component: ListComponent},
      {path: 'auction', component: AuctionComponent},
      { path: '', redirectTo: 'auction', pathMatch: 'full' }
  
];
