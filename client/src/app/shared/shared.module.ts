import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from '../shared/footer/footer.component';
import { DashboardComponent } from '../dashboard/dashboard/dashboard.component';
import { NavbarComponent } from '../shared/navbar/navbar.component';
import { AppRoutingModule } from '../app-routing.module';





@NgModule({
  declarations: [
 
    FooterComponent,
    DashboardComponent,
    NavbarComponent
   ],
  imports: [
    CommonModule,
    AppRoutingModule,
  
  ],
  exports: [
   
    FooterComponent,
    DashboardComponent,
    NavbarComponent
  ]
})
export class SharedModule { }
