import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { AuctionService } from 'src/app/feeds/auctio.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  user: string;
  constructor(public _auth: AuthService, private _auction: AuctionService ) {

    this.getUser();

    this._auction.user$.subscribe(item => this.user = item);
   }
  
  ngOnInit() {
    
  }

  getUser(){
    this.user = localStorage.getItem('user');
  }

}
