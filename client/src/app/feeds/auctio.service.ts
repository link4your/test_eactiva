

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { auction} from '../model/auction.model';
import { of, Observable, Subject } from 'rxjs';
import { mergeMap, catchError} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuctionService {
  userSubject = new Subject<any>();
  user$ = this.userSubject.asObservable();

  constructor(private http: HttpClient) { }


 add(feed: auction, file: File){
  return this.http.post(environment.url + 'auction', feed).pipe( mergeMap( (res: auction) => this.updateImage(res, file)   ));
 }


 update(feed: auction, file: File){
  return this.http.put(environment.url + 'auction/' + feed.id, feed).pipe( mergeMap( (res: auction) => this.updateImage(res, file)   ));
 }


 list(){
   return this.http.get(environment.url + 'auction');
 }

 listOne(id: string){
  return this.http.get(environment.url + 'one/' + id);
 }


 delete(id: string){
  return this.http.delete(environment.url + 'auction/' + id);
 }


 updateImage(feed: auction, file: File) {
  if(!file){
     return of(feed);
   }
   let formData = new FormData();
   formData.append('imagen', file, file.name);
   
   const header = new Headers();
   let request = new Request(environment.url + 'feeds/' + feed, {
     method: 'PATCH',
     headers: header,
     body: formData
   });
   return fetch(request)
   
}


updateFeed(){
  this.http.get(environment.url + 'feeds/update/feed').subscribe(item => {
 console.log(item);
  });
}



setUser(user: string){
this.userSubject.next(user);
}

getUser(){
  return localStorage.getItem('user');
}


setPrice(price: number, id: any) {
  return this.http.post(environment.url + 'price', {price : price, user: this.getUser(), id: id});
}




}
