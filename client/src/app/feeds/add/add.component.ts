
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {Auction, auction } from 'src/app/model/auction.model';
import { NgForm } from '@angular/forms';
import { AuctionService } from '../auctio.service';
import { Router, ActivatedRoute, Routes } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {
  data: Auction = new Auction();
  pictureFile: File;
  @ViewChild('image', {  static: true}) image: ElementRef;
  @ViewChild('deposit', {static: true}) deposit: ElementRef;

  constructor(private _auction: AuctionService, private router: Router, private route: ActivatedRoute) { 
    this.route.params.subscribe(param => {
      if(param.id){
        this. _auction.listOne(param.id).subscribe((item: any) => {
        this.data = new Auction(item);
        
  
        })
      }
    })
  }

  ngOnInit() {
  }

  openType(){
    if(this.data.type === 'special') {
      this.deposit.nativeElement.hidden = false;
    } else {
      this.deposit.nativeElement.hidden = true;
    }
  }


  onSubmit(form: NgForm) {
    if(form.valid) {

     if(!this.data.id) {
      this._auction.add(form.value, this.pictureFile).subscribe(item => {
        
        if(item) {
          
         this.router.navigate(['/auction/list']);
        } else {
          Swal.fire('Alert ','Im sorry, there was a problem', 'warning');
        }
       });
     } else {
      form.value.id= this.data.id;
      this._auction.update(form.value, this.pictureFile).subscribe(item => {
        if(item) {
         this.router.navigate(['/auction/list']);
        } else {
          Swal.fire('Alert ','Im sorry, there was a problem', 'warning');
        }
       });

     }

  
    }
  }

 openImage(){
  this.image.nativeElement.click();
 }


  addImage(event){
  const file = event.target.files[0];
  const reader = new FileReader();
  this.pictureFile = file
  reader.readAsDataURL(file);
  reader.onload = (evt) => {
   //  this.data.img = <string>reader.result;
  }

  }

}
