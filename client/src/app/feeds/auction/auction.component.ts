
import { Component, OnInit, ViewChild,ViewChildren, ElementRef, QueryList } from '@angular/core';
import { AuctionService } from '../auctio.service';
import { Auction, auction } from 'src/app/model/auction.model';
import { Observable, Subject } from 'rxjs';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-auction',
  templateUrl: './auction.component.html',
  styleUrls: ['./auction.component.scss']
})
export class AuctionComponent implements OnInit {
 user: string; 
 auctions: auction[] = [];
 bid_money: number;
  
 @ViewChildren('userchild') userchild: QueryList<ElementRef>;
  constructor(private _auction: AuctionService) { 
    this._auction.list().subscribe((item: auction[]) => {
      const today = new Date();
      item.forEach((element: any) => {
         const startDate = new Date(element.startDate.date);
         const endDate = new Date(element.endDate.date);
       
        if(startDate < today && endDate > today) {

          this.auctions.push(element);
        }


      });
     });
  }


  ngOnInit() {
  }


  bid(index: number){
    if(this.getUser()){
     
      const actually_price = this.auctions[index].price;
      const id =  this.auctions[index].id;
      if(this.bid_money > actually_price){
     
        this._auction.setPrice(this.bid_money, id).subscribe( item => {
         if(item) {
           Swal.fire(`Enhorabuena ${this.user}`, `Has pujado ${this.bid_money} euros` );
          this.auctions[index].price = this.bid_money;
          this.bid_money =0;
         } else {
          Swal.fire('Alerta', `Ha ocurrido algun error`, 'warning');
         }
        });

      } else {

        Swal.fire('Alerta', `Puja demasiado baja, la puja actual es de ${actually_price}`, 'warning');
      }


    } else {
     this.userchild.toArray()[index].nativeElement.hidden = false;
    }
  }

  getUser(){
    const user = localStorage.getItem('user');
    if(user !== undefined && user !== null) {
      this.user = user;
      return true;
    } else {
      return false;
    }
  }

  setUser(index: number){
    localStorage.setItem('user', this.user);
    this.userchild.toArray()[index].nativeElement.hidden = true;
    this._auction.setUser(this.user);
  }






}
