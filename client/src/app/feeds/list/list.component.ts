import { Component, OnInit } from '@angular/core';
import { AuctionService } from '../auctio.service';
import { auction } from 'src/app/model/auction.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  feeds:auction[] = []; 
  constructor(private _feed: AuctionService) { 
    this._feed.list().subscribe((item: auction[]) => this.feeds = item);
  }

  ngOnInit() {
  }


 delete(id: string){
   if(id){
     this._feed.delete(id).subscribe(item => {
     if(item) {
          this.feeds = this.feeds.filter(item => item.id != id);
          Swal.fire('Success','the feed was deleted with exist', 'success');
     } else {
      Swal.fire('Alert ','Im sorry, there was a problem', 'warning');
     }
     });
   }
 }

 

}
