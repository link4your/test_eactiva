import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddComponent} from './add/add.component';
import { ListComponent } from './list/list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PipesModule } from '../pipes/pipes/pipes.module';
import { AuctionComponent } from './auction/auction.component';






@NgModule({
  declarations: [
    AddComponent,
    ListComponent,
    AuctionComponent,
  
  ],
  imports: [
    CommonModule,
    FormsModule, 
    ReactiveFormsModule,
    RouterModule,
    PipesModule
  ],
  exports: [
    AddComponent,
    ListComponent,
 
  ]
})
export class AuctionModule { }
