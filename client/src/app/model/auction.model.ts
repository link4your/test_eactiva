export class Auction {

    id?: string;
    title: string;
    startDate: Date;
    endDate: Date;
    deposit?: number;
    price: number;
    type: string;
    



    constructor(data?: auction){
        this.id = data && data.id || null;
        this.title = data && data.title || null;
        this.startDate = data && data.startDate || new Date();
        this.endDate = data && data.endDate || new Date();
        this.deposit = data && data.deposit || null;
        this.price = data && data.price || null;
        this.type = data && data.type || 'regular';
        }


}

export interface auction {
    id?: string,
    title: string,
    startDate: Date,
    endDate: Date,
    deposit?: number,
    price: number,
    type: string
}
