-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.10-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para eactiva
DROP DATABASE IF EXISTS `eactiva`;
CREATE DATABASE IF NOT EXISTS `eactiva` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `eactiva`;

-- Volcando estructura para tabla eactiva.auction
DROP TABLE IF EXISTS `auction`;
CREATE TABLE IF NOT EXISTS `auction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_type_id` int(11) DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `price` int(11) DEFAULT NULL,
  `deposit` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DEE4F5931BD125E3` (`id_type_id`),
  CONSTRAINT `FK_DEE4F5931BD125E3` FOREIGN KEY (`id_type_id`) REFERENCES `auction_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla eactiva.auction: ~3 rows (aproximadamente)
DELETE FROM `auction`;
/*!40000 ALTER TABLE `auction` DISABLE KEYS */;
INSERT INTO `auction` (`id`, `id_type_id`, `start_date`, `end_date`, `price`, `deposit`, `title`) VALUES
	(1, 2, '2019-12-12 00:00:00', '2019-12-18 00:00:00', 4000, 0, 'prueba de pie'),
	(4, 1, '2019-12-14 00:00:00', '2019-12-20 00:00:00', 100, 1000, 'consentiento asociado'),
	(5, 1, '2019-12-13 00:00:00', '2019-12-14 00:00:00', 0, 500, 'prueba de pie'),
	(6, 1, '2019-12-17 00:00:00', '2019-12-25 00:00:00', 0, 10, 'prueba de pie');
/*!40000 ALTER TABLE `auction` ENABLE KEYS */;

-- Volcando estructura para tabla eactiva.auction_type
DROP TABLE IF EXISTS `auction_type`;
CREATE TABLE IF NOT EXISTS `auction_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla eactiva.auction_type: ~2 rows (aproximadamente)
DELETE FROM `auction_type`;
/*!40000 ALTER TABLE `auction_type` DISABLE KEYS */;
INSERT INTO `auction_type` (`id`, `name`) VALUES
	(1, 'special'),
	(2, 'regular');
/*!40000 ALTER TABLE `auction_type` ENABLE KEYS */;

-- Volcando estructura para tabla eactiva.migration_versions
DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla eactiva.migration_versions: ~2 rows (aproximadamente)
DELETE FROM `migration_versions`;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
	('20191208093944', '2019-12-08 09:39:53'),
	('20191208095034', '2019-12-08 09:51:08'),
	('20191208120651', '2019-12-08 12:06:58');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;

-- Volcando estructura para tabla eactiva.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  UNIQUE KEY `UNIQ_8D93D6497BA2F5EB` (`api_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla eactiva.user: ~0 rows (aproximadamente)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `email`, `roles`, `password`, `api_token`) VALUES
	(1, 'admin@gmail.com', 'ROLE_ADMIN', '$argon2id$v=19$m=65536,t=4,p=1$RS85bnUxckJ4aUF1UDZGRA$puTrsPyEbs1kYfWV8Y0Ql5COJ5v3TAvus0xdgyisyms', NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
