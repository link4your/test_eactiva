<?php
// src/Controller/AdminController.php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Auction;
use App\Entity\AuctionType;

class AdminController extends AbstractController
{
    public function addAuction(Request $request)
    {

        $json = json_decode($request->getContent());

         if($json->title && $json->startDate && $json->endDate && $json->type) {

        try {
             $em = $this->getDoctrine()->getManager();
            $repository = $em->getRepository(Auction::class);

            $idType = $em->getRepository(AuctionType::class)->findOneBy(['name'=> $json->type]);
            $auction = new Auction();
            $auction->setTitle($json->title);
            $auction->setStartDate(new \DateTime($json->startDate));
            $auction->setEndDate(new \DateTime($json->endDate));
            $auction->setIdType($idType);
            $auction->setPrice($json->price);
            $auction->setDeposit($json->deposit);

            $em->persist($auction);
            $em->flush();
            $em->close();

            return new JsonResponse('ok');
            } catch(\Exception $e){
                return  new JsonResponse($e);

            }
         } else {
            return new JsonResponse($json->title);

        }
       
    }


    public function updateAuction(Request $request, $id){
        $json = json_decode($request->getContent());

        if($json->title && $json->startDate && $json->endDate && $json->type) {

           $em = $this->getDoctrine()->getManager();


           try {
                $repository = $em->getRepository(Auction::class);

                $idType = $em->getRepository(AuctionType::class)->findOneBy(['name'=> $json->type]);
                $auction = $em->getRepository(Auction::class)->find($id);
                $auction->setTitle($json->title);
                $auction->setStartDate(new \DateTime($json->startDate));
                $auction->setEndDate(new \DateTime($json->endDate));
                $auction->setIdType($idType);
                $auction->setPrice($json->price);
                $auction->setDeposit($json->deposit);
    
                $em->persist($auction);
                $em->flush();
                $em->close();
 
                 return new JsonResponse('ok');
            } catch(\Exception $e){
                return new JsonResponse($e);
            
           }
        
    } else {
           return new JsonResponse($json->title);

       }
    }


    

    public function deleteAuction(Request $request, $id){
        if($id) {
            $em = $this->getDoctrine()->getManager();
            try {

                $auction = $em->getRepository(Auction::class)->find($id);
                $em->remove($auction);
                $em->flush();
                $em->close();
                return new JsonResponse(true);

            } catch(\Exception $e) {
                return new JsonResponse($e);
            }
          
    
        } else {
            return new JsonResponse(false);
        }
       
       
    }


    public function getAuction(){

        try {

            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
            "SELECT p.title,
                p.startDate,
                p.endDate, p.price, p.deposit, p.id
                FROM App\Entity\Auction p
                "
            );
            $data = $query->getResult();
            return new JsonResponse($data);
                
        } catch(\Exception $e) {
            return new JsonResponse($e);
        }  


      

    }

public function getOneAuction(Request $request, $id){


    if($id) {

        try {

            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery(
                "SELECT p.title,  p.price, p.id ,
                    DATE_FORMAT(p.startDate, '%Y-%m-%d') AS startDate, 
                    DATE_FORMAT(p.endDate, '%Y-%m-%d') AS endDate 
                    FROM App\Entity\Auction p WHERE p.id= :id
                    "
                )->setParameter('id' , $id);
                $data = $query->getOneOrNullResult();
                return new JsonResponse($data);
                    
            } catch(\Exception $e) {
                return new JsonResponse($e);
            }

        
    }
    return new JsonResponse(false);
    
}


public function addPrice(Request $request){
    $json = json_decode($request->getContent());
    if($json->price && $json->user && $json->id) {
        try {

            $em = $this->getDoctrine()->getManager();
            $auction = $em->getRepository(Auction::class)->find($json->id);
            $auction->setPrice($json->price);
            $em->persist($auction);
            $em->flush();
            $em->close();
            return new JsonResponse(true);
                    
        } catch(\Exception $e) {
            return new JsonResponse($e);
        }
      
        
    } else {

        return new JsonResponse(false);
    }
}


}