<?php
// src/Controller/SecurityController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class SecurityController extends AbstractController
{
   
    public function login(Request $request, JWTTokenManagerInterface $JWTManager): Response
    {  
       
        $user =  $request->request->get('username') ?? false;
        $password =$request->request->get('password') ?? false;

        if ($user && $password) {
         
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository(User::class);
                  $userRepository = $repository->findOneBy(['email'=> $user]);
                if ($userRepository) {

                    $iguales = password_verify($password, $userRepository->getPassword());
                   
                    if ($iguales) {
                               return new JsonResponse(['token' => $JWTManager->create($userRepository)]);

                    } else {

                        return new JsonResponse('error');
                    }
                }
            } catch (\Doctrine\DBAL\Exception $e) {
                return new JsonResponse('error');
            }

            return new JsonResponse('error 2');
        }


        return new JsonResponse('error 3');
       
    }
}