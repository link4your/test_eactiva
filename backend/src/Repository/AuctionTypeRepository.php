<?php

namespace App\Repository;

use App\Entity\AuctionType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method AuctionType|null find($id, $lockMode = null, $lockVersion = null)
 * @method AuctionType|null findOneBy(array $criteria, array $orderBy = null)
 * @method AuctionType[]    findAll()
 * @method AuctionType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuctionTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AuctionType::class);
    }

    // /**
    //  * @return AuctionType[] Returns an array of AuctionType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AuctionType
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
